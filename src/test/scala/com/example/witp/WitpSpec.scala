package com.example.witp

import Main._

class WitpSpec extends org.specs2.mutable.Specification {

  "divble function" should {

    def divbleTest(factor: Int, divble: Pred) = {
      "return true for" in {
        "0" in (divble(0) must beTrue)
        "factor" in (divble(factor) must beTrue)
        "product" in (divble(factor * 2) must beTrue)
      }
      "return false for" in {
        "1" in (divble(1) must beFalse) // test does not apply to factor of 1
        "greater" in (divble(factor + 1) must beFalse)
        "smaller" in (divble(factor - 1) must beFalse)
      }
    }

    "work for 4" in divbleTest(4, divble4)
    "work for 5" in divbleTest(5, divble5)
  }

  "translate" should {

    def expected(i: Int): String = {
      if (i % 20 == 0) "HelloWorld"
      else if (i % 4 == 0) "Hello"
      else if (i % 5 == 0) "World"
      else i.toString
    }

    "work " in {
      examplesBlock {
        for (i <- 0 to 100) {
          s"print ${expected(i)} for $i" in { translate(i) must beEqualTo(expected(i)) }
        }
      }
    }
  }
}
