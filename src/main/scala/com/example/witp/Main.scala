package com.example.witp

object Main extends App {

  type Pred = Int => Boolean
  private lazy val divble: Int => Pred = i => _ % i == 0
  lazy val divble4 = divble(4)
  lazy val divble5 = divble(5)

  lazy val translate: Int => String = { i =>
    if (divble4(i))
      if (divble5(i)) "HelloWorld"
      else "Hello"
    else if (divble5(i)) "World"
    else i.toString
  }

  (1 to 100).map(translate).foreach(println)

}
